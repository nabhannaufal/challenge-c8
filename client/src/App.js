import React, { useEffect, useState } from "react";
import * as axios from "axios";
import "./mystyle.css";

function App() {
  const [data, setData] = useState([]);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [id, setId] = useState(null);
  const [usernameSearch, setUsernameSearch] = useState("");
  const [emailSearch, setEmailSearch] = useState("");
  const [idSearch, setIdSearch] = useState("");
  const [dataSearch, setDataSearch] = useState([]);

  useEffect(async () => {
    const result = await axios("http://localhost:5000/api/players");
    setData(result.data.message);
  }, [data]);

  const save = async () => {
    const saveData = await axios.post("http://localhost:5000/api/players", {
      username,
      email,
      password,
    });

    if (saveData.status === 201) {
      const currentData = data;
      currentData.push(saveData.data.message);
      setData(currentData);
    }
  };

  const del = async (key) => {
    const dataDel = data[key];
    setId(dataDel.id);
    const deleteData = await axios.delete(
      `http://localhost:5000/api/players/${id}`
    );
  };

  const edit = (key) => {
    const dataEdit = data[key];
    setUsername(dataEdit.username);
    setEmail(dataEdit.email);
    setPassword(dataEdit.password);
    setId(dataEdit.id);
  };

  const update = async () => {
    const updateData = await axios.put(
      `http://localhost:5000/api/players/${id}`,
      {
        username,
        email,
        password,
      }
    );
  };

  const search = () => {
    const currentDataSearch = data.filter(
      (item) =>
        item.username.includes(usernameSearch) ||
        item.email.includes(emailSearch) ||
        item.id.includes(idSearch)
    );
    setDataSearch(currentDataSearch);
  };

  return (
    <div className="App">
      <body>
        <div class="centerCont">
          <div class="judul" id="img">
            <h1 class="paralax">List Player</h1>
          </div>
        </div>
        <div class="centerCont">
          <div class="listPlayer" id="img">
            <table class="table">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {data?.map((item, key) => (
                  <tr>
                    <th>{item.username}</th>
                    <th>{item.email}</th>
                    <th>
                      <button
                        class="btn-edit"
                        id={key}
                        onClick={(e) => edit(e.target.id)}
                      >
                        edit
                      </button>
                      <button
                        class="btn-del"
                        id={key}
                        onClick={(e) => del(e.target.id)}
                      >
                        delete
                      </button>
                    </th>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div class="centerCont">
          <div class="card" id="img">
            <h2>Add/Edit player</h2>
            <input
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <br />
            <input
              type="email"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <br />
            <input
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <button onClick={save} class="btn">
              Add
            </button>
            <button onClick={update} class="btn">
              Edit
            </button>
          </div>
          <div class="card" id="img">
            <h2>Search player</h2>
            <input
              type="text"
              placeholder="By Username"
              value={usernameSearch}
              onChange={(e) => setUsernameSearch(e.target.value)}
            />
            <br />
            <input
              type="text"
              placeholder="By Email"
              value={emailSearch}
              onChange={(e) => setEmailSearch(e.target.value)}
            />
            <br />
            <input
              type="text"
              placeholder="By Id"
              value={idSearch}
              onChange={(e) => setIdSearch(e.target.value)}
            />
            <br />
            <button onClick={search} class="btn">
              Search
            </button>
          </div>
        </div>
        <div class="centerCont">
          <div class="listPlayer" id="img">
            <table class="table">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Username</th>
                  <th>Email</th>
                </tr>
              </thead>
              <tbody>
                {dataSearch?.map((item) => (
                  <tr>
                    <th>{item.id}</th>
                    <th>{item.username}</th>
                    <th>{item.email}</th>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </body>
    </div>
  );
}

export default App;
