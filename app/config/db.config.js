module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "nabhan",
  DB: "nabhan_chapter8",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
